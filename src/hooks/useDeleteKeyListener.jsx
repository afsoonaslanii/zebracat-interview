import { useEffect } from 'react';

/**
 * Custom hook to listen for 'Delete' key presses and execute a callback function.
 * @param {function} callback - Callback function to be executed on 'Delete' key press.
 */
const useDeleteKeyListener = (callback) => {
  useEffect(() => {
    const handleKeyDown = (event) => {
      if (event.key === 'Delete') {
        callback();
      }
    };

    window.addEventListener('keydown', handleKeyDown);

    return () => {
      window.removeEventListener('keydown', handleKeyDown);
    };
  }, [callback]);
};

export default useDeleteKeyListener;
