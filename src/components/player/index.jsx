import { Player as RemotionPlayer } from '@remotion/player';
import { useCallback } from 'react';
import { AbsoluteFill } from 'remotion';
import { FIXED_VIDEO_HEIGHT } from '../../constants';
import { CustomComposition } from '../customComposition';
import Loader from '../loader';

const Player = ({ inputProps }) => {
  const renderLoading = useCallback(() => {
    return (
      <AbsoluteFill>
        <Loader />
      </AbsoluteFill>
    );
  }, []);

  return (
    <RemotionPlayer
      loop
      controls
      autoPlay
      component={CustomComposition}
      fps={30}
      durationInFrames={150}
      compositionWidth={700}
      clickToPlay={false}
      renderLoading={renderLoading}
      inputProps={inputProps}
      compositionHeight={FIXED_VIDEO_HEIGHT}
      style={{
        width: '100%',
        height: '100%',
        margin: '0 auto',
        background: 'gray',
      }}
    />
  );
};

export default Player;
