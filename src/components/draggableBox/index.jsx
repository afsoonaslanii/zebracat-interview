import { useState, useRef } from 'react';
import './draggableBox.css';

/**
 * DraggableBox component allows its children to be dragged around the screen.
 *
 * @param {React.ReactNode} props.children - The content to be rendered inside the draggable box.
 * @param {number} props.highestZIndex - The highest z-index among all draggable components
 * @param {Function} props.setHighestZIndex - Function to update the highest z-index
 * @returns {JSX.Element} The draggable box component.
 */
const DraggableBox = ({ children, highestZIndex, setHighestZIndex }) => {
  const [position, setPosition] = useState({ x: 0, y: 0 });
  const [isDragging, setIsDragging] = useState(false);
  const [zIndex, setZIndex] = useState(1);

  // Store the initial drag start position
  const dragStart = useRef({ x: 0, y: 0 });

  const handleMouseDown = (e) => {
    e.preventDefault();
    e.stopPropagation();
    setIsDragging(true);

    // Bring the component to the top and Update the highest z-index
    setZIndex(highestZIndex + 1);
    setHighestZIndex?.(highestZIndex + 1);

    // Record the initial position where the mouse is clicked relative to the box
    dragStart.current = {
      x: e.clientX - position.x,
      y: e.clientY - position.y,
    };
    // Follow mouse move and mouse up events
    document.addEventListener('mousemove', handleMouseMove);
    document.addEventListener('mouseup', handleMouseUp);
  };

  const handleMouseMove = (e) => {
    // Calculate the new position of the box
    const newX = e.clientX - dragStart.current.x;
    const newY = e.clientY - dragStart.current.y;

    // Update the box's position state
    setPosition({ x: newX, y: newY });
  };

  const handleMouseUp = () => {
    setIsDragging(false);
    document.removeEventListener('mousemove', handleMouseMove);
    document.removeEventListener('mouseup', handleMouseUp);
  };

  return (
    <div
      onMouseDown={handleMouseDown}
      className={`draggable-box ${isDragging ? 'dragging' : ''} `}
      style={{
        position: 'absolute',
        zIndex,
        left: `${position.x}px`,
        top: `${position.y}px`,
      }}>
      {children}
    </div>
  );
};

export default DraggableBox;
