import './container.css';

/**
 * Container component.
 *
 * This component serves as a wrapper that ensures consistent padding for its children elements.
 *
 * @param {React.ReactNode} props.children - The content to be wrapped by the container.
 * @param {string} [props.className=''] - Additional class to apply to the container.
 *
 * @returns {JSX.Element} The rendered container component.
 */
const Container = ({ children, className = '' }) => {
  return <div className={`container ${className}`}>{children}</div>;
};

export default Container;
