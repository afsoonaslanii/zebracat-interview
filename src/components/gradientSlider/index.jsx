import { useState, useRef, useEffect } from 'react';
import { GRADIENT_DIRECTIONS } from '../../constants';
import './gradientSlider.css';

/**
 * GradientSlider component that allows users to select colors and positions to create a gradient.
 * Users can also select the direction of the gradient.
 * @returns {JSX.Element} The rendered GradientSlider component.
 */
const GradientSlider = ({ onChangeGradient }) => {
  const startThumbRef = useRef(null);
  const endThumbRef = useRef(null);
  const sliderRef = useRef(null);
  const [startValue, setStartValue] = useState(0);
  const [endValue, setEndValue] = useState(100);
  const [startColor, setStartColor] = useState('#17b55c');
  const [endColor, setEndColor] = useState('#d18400');
  const [gradientDirection, setGradientDirection] = useState('90');
  const [isDragging, setIsDragging] = useState(false);
  const [activeThumb, setActiveThumb] = useState(null);

  /**
   * move event to update the position of the active thumb.
   */
  const handleMouseDown = (thumb) => {
    setIsDragging(true);
    setActiveThumb(thumb);
  };

  const handleMouseUp = () => {
    setIsDragging(false);
    setActiveThumb(null);
  };

  const handleMouseMove = (e) => {
    if (!isDragging || !activeThumb) return;

    const sliderRect = sliderRef.current.getBoundingClientRect();
    const newValue = ((e.clientX - sliderRect.left) / sliderRect.width) * 100;
    if (activeThumb === 'start' && newValue < endValue && newValue >= 0) {
      setStartValue(newValue);
      onChangeGradient({ gradient: gradientVal() });
    } else if (
      activeThumb === 'end' &&
      newValue > startValue &&
      newValue <= 100
    ) {
      setEndValue(newValue);
      onChangeGradient({ gradient: gradientVal() });
    }
  };

  useEffect(() => {
    document.addEventListener('mousemove', handleMouseMove);
    document.addEventListener('mouseup', handleMouseUp);
    return () => {
      document.removeEventListener('mousemove', handleMouseMove);
      document.removeEventListener('mouseup', handleMouseUp);
    };
  }, [isDragging, activeThumb, startValue, endValue]);

  const handleChangeColor = (position, color) => {
    if (position === 'start') {
      setStartColor(color);
    } else if (position === 'end') {
      setEndColor(color);
    }
    onChangeGradient({ gradient: gradientVal() });
  };

  const handleChangeDirection = (deg) => {
    setGradientDirection(deg);
    onChangeGradient({ gradient: gradientVal() });
  };

  /**
   * Handles click event on the slider to move the nearest thumb.
   */
  const handleSliderClick = (e) => {
    const sliderRect = sliderRef.current.getBoundingClientRect();
    const clickPosition =
      ((e.clientX - sliderRect.left) / sliderRect.width) * 100;

    const startDifference = Math.abs(clickPosition - startValue);
    const endDifference = Math.abs(clickPosition - endValue);

    if (
      startDifference < endDifference &&
      clickPosition < endValue &&
      clickPosition >= 0
    ) {
      setStartValue(clickPosition);
      onChangeGradient({ gradient: gradientVal() });
    } else if (clickPosition > startValue && clickPosition <= 100) {
      setEndValue(clickPosition);
      onChangeGradient({ gradient: gradientVal() });
    }
  };

  /**
   * Calculates the gradient CSS string based on the selected colors, positions, and direction.
   */
  const gradientVal = () =>
    `${gradientDirection}deg, ${startColor} ${startValue}%, ${endColor} ${endValue}%`;
  const calculateGradient = () => {
    return `linear-gradient(${gradientVal()})`;
  };

  return (
    <div className='gradient-slider-container'>
      <div className='gradient-col'>
        <div
          className='slider-track'
          style={{ background: calculateGradient() }}
          ref={sliderRef}
          onClick={handleSliderClick}>
          <div
            ref={startThumbRef}
            className='slider-thumb start-thumb'
            style={{ left: `${startValue}%` }}
            onMouseDown={() => handleMouseDown('start')}
            onTouchStart={() => handleMouseDown('start')}></div>
          <div
            ref={endThumbRef}
            className='slider-thumb end-thumb'
            style={{ left: `${endValue}%` }}
            onMouseDown={() => handleMouseDown('end')}
            onTouchStart={() => handleMouseDown('end')}></div>
        </div>
      </div>
      <div className='gradient-col'>
        <input
          type='color'
          value={startColor}
          className='color-input'
          onChange={(e) => handleChangeColor('start', e.target.value)}
        />
        <select
          className='direction-select'
          value={gradientDirection}
          onChange={(e) => handleChangeDirection(e.target.value)}>
          {GRADIENT_DIRECTIONS.map((direction) => (
            <option key={direction.value} value={direction.value}>
              {direction.label}
            </option>
          ))}
        </select>
        <input
          type='color'
          value={endColor}
          className='color-input'
          onChange={(e) => handleChangeColor('end', e.target.value)}
        />
      </div>
    </div>
  );
};

export default GradientSlider;
