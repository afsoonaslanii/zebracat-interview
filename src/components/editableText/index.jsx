import { useState, useRef, useCallback } from 'react';
import { useOutsideClick } from '../../hooks/useOutsideClick';
import './editableText.css';

/**
 * EditableText component allows users to double-click on text to edit it.
 * @param {Object} props.fontStyle - Font settings for the text.
 * @param {Object} props.value - Input Value.
 * @param {Object} props.handleChangeText - allow to change input value.
 * @returns {JSX.Element} The EditableText component.
 */
const EditableText = ({ fontStyle, value, onClick, handleChangeText }) => {
  const inputRef = useRef(null);
  const spanRef = useRef(null);

  const [isEditing, setIsEditing] = useState(false);

  // Custom hook to detect clicks outside the input element to exit editing mode
  useOutsideClick(inputRef, () => setIsEditing(false));

  /**
   * Handles the double-click event to enable editing mode.
   */
  const handleDoubleClick = useCallback(() => {
    setIsEditing(true);
  }, []);

  /**
   * Handles the key press event, specifically to exit editing mode on 'Enter' key press.
   */
  const handleKeyPress = useCallback((event) => {
    if (event.key === 'Enter') {
      setIsEditing(false);
    }
  }, []);

  /**
   * Handles the change event for the input element to update the text value.
   */
  const handleChange = (event) => {
    handleChangeText({ value: event.target.value });
  };

  const colorMaker = () => {
    if (fontStyle?.gradient) {
      return { background: `-webkit-linear-gradient(${fontStyle.gradient})` };
    } else {
      return { color: fontStyle.color };
    }
  };

  return (
    <>
      {isEditing ? (
        <input
          autoFocus
          type='text'
          className={`editable-text ${fontStyle.gradient ? 'gradient-text' : ''}`}
          ref={inputRef}
          value={value || ''}
          onChange={handleChange}
          onKeyPress={handleKeyPress}
          onDoubleClick={() => inputRef.current.select()}
          style={{
            ...fontStyle,
            ...colorMaker(),
            fontSize: `${fontStyle.fontSize}px`,
          }}
        />
      ) : (
        <span
          ref={spanRef}
          className={`${fontStyle.gradient ? 'gradient-text' : ''}`}
          style={{
            ...fontStyle,
            ...colorMaker(),
            fontSize: `${fontStyle.fontSize}px`,
          }}
          onClick={onClick}
          onDoubleClick={handleDoubleClick}>
          {value}
        </span>
      )}
    </>
  );
};

export default EditableText;
