import './editableText.css';
import { useState, useRef, useCallback } from 'react';
import { useOutsideClick } from '../../hooks/useOutsideClick';
import DraggableBox from '../draggableBox';
import { spring, useCurrentFrame, useVideoConfig } from 'remotion';

/**
 * TextMotion component allows users to double-click on text to edit it with animation and draggable functionality.
 * @param {Object} props.fontStyle - Font settings for the text.
 * @param {Object} props.value - Input Value.
 * @param {Object} props.handleChangeText - allow to change input value.
 * @param {number} props.highestZIndex - Current highest z-index value.
 * @param {Function} props.setHighestZIndex - Function to set the highest z-index value.
 * @returns {JSX.Element} The TextMotion component.
 */
const TextMotion = ({
  fontStyle,
  value,
  onClick,
  handleChangeText,
  highestZIndex,
  setHighestZIndex,
}) => {
  const inputRef = useRef(null);
  const spanRef = useRef(null);

  const [isEditing, setIsEditing] = useState(false);

  // Custom hook to detect clicks outside the input element to exit editing mode
  useOutsideClick(inputRef, () => setIsEditing(false));

  /**
   * Handles the double-click event to enable editing mode.
   */
  const handleDoubleClick = useCallback(() => {
    setIsEditing(true);
  }, []);

  /**
   * Handles the key press event, specifically to exit editing mode on 'Enter' key press.
   */
  const handleKeyDown = useCallback((event) => {
    if (event.key === 'Enter') {
      setIsEditing(false);
    }
  }, []);

  /**
   * Handles the change event for the input element to update the text value.
   */
  const handleChange = (event) => {
    handleChangeText({ value: event.target.value });
  };

  const colorMaker = () => {
    if (fontStyle?.gradient) {
      return { background: `-webkit-linear-gradient(${fontStyle.gradient})` };
    } else {
      return { color: fontStyle.color };
    }
  };

  const videoConfig = useVideoConfig();
  const frame = useCurrentFrame();
  const words = value.split(' ');

  return (
    <>
      <DraggableBox
        highestZIndex={highestZIndex}
        setHighestZIndex={setHighestZIndex}>
        {isEditing ? (
          <input
            autoFocus
            type='text'
            className={`editable-text ${fontStyle.gradient ? 'gradient-text' : ''}`}
            ref={inputRef}
            value={value || ''}
            onChange={handleChange}
            onKeyDown={handleKeyDown}
            onDoubleClick={() => inputRef.current.select()}
            style={{
              ...fontStyle,
              ...colorMaker(),
              fontSize: `${fontStyle.fontSize}px`,
            }}
          />
        ) : (
          <div>
            {words?.map((t, i) => {
              const delay = i * 50;

              const scale = spring({
                fps: videoConfig.fps,
                frame: frame - delay,
                config: {
                  damping: 200,
                },
              });

              return (
                <span
                  key={t}
                  ref={spanRef}
                  className={`word ${fontStyle.gradient ? 'gradient-text' : ''}`}
                  onClick={onClick}
                  onDoubleClick={handleDoubleClick}
                  style={{
                    ...fontStyle,
                    ...colorMaker(),
                    fontSize: `${fontStyle.fontSize}px`,
                    transform: `scale(${scale})`,
                  }}>
                  {t}
                </span>
              );
            })}
          </div>
        )}
      </DraggableBox>
    </>
  );
};

export default TextMotion;
