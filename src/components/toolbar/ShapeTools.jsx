import { IconRectangle, IconCircle, IconSquare } from '@tabler/icons-react';
import { CIRCLE, RECT, SQUARE } from '../../constants';

const ShapeTools = ({ shapeStyle, handleAddShape, handleChangeShapeStyle }) => {
  const items = {
    [SQUARE]: <IconSquare />,
    [RECT]: <IconRectangle />,
    [CIRCLE]: <IconCircle />,
  };
  return (
    <div className='left-sidebar-item'>
      <label>
        shape
        <div className='shape-list'>
          {Object.entries(items).map(([key, shape]) => {
            return (
              <div
                key={key}
                className='shape-item'
                onClick={() => handleAddShape(key)}>
                {shape}
              </div>
            );
          })}
        </div>
      </label>

      <hr className='divider' />

      <label>
        color
        <input
          type='color'
          name='color'
          className='solo-color'
          value={shapeStyle?.fill}
          onChange={(event) =>
            handleChangeShapeStyle({ fill: event.target.value })
          }
        />
      </label>
    </div>
  );
};

export default ShapeTools;
