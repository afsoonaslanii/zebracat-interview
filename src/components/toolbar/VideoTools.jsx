import { ASPECT_RATIOS } from '../../constants';

/**
 * This component provides tools for adjusting the video settings, specifically the aspect ratio.
 *
 * @param {string} props.aspectRatio - The current video aspect ratio.
 * @param {Function} props.handleAspectRatioChange - Function to handle changes to the video aspect ratio.
 *
 * @returns {JSX.Element} The rendered VideoTools component.
 */
const VideoTools = ({ aspectRatio, handleAspectRatioChange }) => {
  return (
    <div className='left-sidebar-item'>
      <label>
        aspect ratio
        <select
          name='aspectRatio'
          value={aspectRatio}
          onChange={handleAspectRatioChange}>
          {ASPECT_RATIOS.map((ratio) => {
            return (
              <option key={ratio} value={ratio}>
                {ratio}
              </option>
            );
          })}
        </select>
      </label>
    </div>
  );
};

export default VideoTools;
