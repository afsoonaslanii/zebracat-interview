import './toolbar.css';
import ToolbarItem from './ToolbarItem';
import Container from '../container';
import { IconTextSize, IconBrandYoutube, IconShape } from '@tabler/icons-react';

/**
 *
 * This component renders a toolbar with various items such as text, shape, and video.
 * It utilizes the `ToolbarItem` component for each item and the `Container` component for layout.
 *
 * @param {Function} props.handleChangeActiveMenu - Function to handle the change of the active menu item.
 * @param {string} props.activeMenu - The currently active menu item name.
 *
 * @returns {JSX.Element} The rendered toolbar component.
 */
const Toolbar = ({ handleChangeActiveMenu, activeMenu }) => {
  const toolbarItems = [
    {
      id: 1,
      name: 'text',
      icon: <IconTextSize size='14' />,
    },
    {
      id: 2,
      name: 'shape',
      icon: <IconShape size='14' />,
    },
    {
      id: 3,
      name: 'video',
      icon: <IconBrandYoutube size='14' />,
    },
  ];

  /**
   * Handles menu item click event.
   */
  const handleOnMenuClick = (item) => {
    handleChangeActiveMenu(item.name);
  };

  return (
    <>
      <Container className='toolbar-container'>
        {toolbarItems.map((item) => (
          <ToolbarItem
            className={item.name === activeMenu ? 'active' : ''}
            key={item.id}
            text={item.name}
            icon={item.icon}
            onClick={() => handleOnMenuClick(item)}
          />
        ))}
      </Container>
    </>
  );
};

export default Toolbar;
