import './toolbar.css';

/**
 * This component represents an individual item in a toolbar.
 * It displays an icon and text, and handles click events.
 *
 * @param {string} props.text - The text to display in the toolbar item.
 * @param {JSX.Element} props.icon - The icon to display in the toolbar item.
 * @param {Function} props.onClick - Function to handle click events on the toolbar item.
 * @param {string} [props.className=''] - Additional class names to apply to the toolbar item.
 *
 * @returns {JSX.Element} The rendered ToolbarItem component.
 */
const ToolbarItem = ({ text, icon, onClick, className }) => {
  return (
    <div className={`${className} toolbar-item`} onClick={onClick}>
      {icon}
      {text}
    </div>
  );
};

export default ToolbarItem;
