import './toolbar.css';
import './inputs.css';
import { IconPlus } from '@tabler/icons-react';
import { FONT_FAMILIES, FONT_WEIGHTS } from '../../constants';
import GradientSlider from '../gradientSlider';

/**
 * This component provides tools for adjusting the font style of text elements.
 * It includes controls for font size, color, gradient, font weight, and font family.
 *
 * @param {Object} props.fontStyle - The current font style settings.
 * @param {Function} props.handleChangeText - Function to handle changes to the text style.
 * @param {Function} props.handleAddText - Function to handle adding new text.
 *
 * @returns {JSX.Element} The rendered FontStyleTools component.
 */
const FontStyleTools = ({ fontStyle, handleChangeText, handleAddText }) => {
  return (
    <div className='left-sidebar-item'>
      <div className='add-text-item'>
        <button className='button' onClick={handleAddText}>
          <IconPlus size='14' /> add text
        </button>
      </div>
      <hr className='divider' />
      <label>
        font size
        <div className='range-slider-div'>
          <input
            step='1'
            min='10'
            max='100'
            type='range'
            name='fontSize'
            className='range-slider'
            value={fontStyle?.fontSize}
            onChange={(event) =>
              handleChangeText({ fontSize: event.target.value })
            }
          />
          <span>{fontStyle?.fontSize ? fontStyle?.fontSize : '16'}px</span>
        </div>
      </label>
      <hr className='divider' />
      <label>
        color
        <input
          type='color'
          name='color'
          className='solo-color'
          value={fontStyle?.color}
          onChange={(event) =>
            handleChangeText({ color: event.target.value, gradient: '' })
          }
        />
      </label>

      <hr className='divider' />
      <label>
        Gradient
        <GradientSlider onChangeGradient={handleChangeText} />
      </label>

      <hr className='divider' />
      <label>
        Font Weight
        <select
          name='fontWeight'
          value={fontStyle?.fontWeight}
          onChange={(event) =>
            handleChangeText({ fontWeight: event.target.value })
          }>
          {FONT_WEIGHTS.map((item) => {
            return (
              <option key={item} value={item}>
                {item}
              </option>
            );
          })}
        </select>
      </label>
      <hr className='divider' />
      <label>
        Font Family
        <select
          name='fontFamily'
          value={fontStyle?.fontFamily}
          onChange={(event) =>
            handleChangeText({ fontFamily: event.target.value })
          }>
          {FONT_FAMILIES.map((item) => {
            return (
              <option key={item} value={item}>
                {item}
              </option>
            );
          })}
        </select>
      </label>
    </div>
  );
};

export default FontStyleTools;
