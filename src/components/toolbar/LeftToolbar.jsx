import './toolbar.css';
import FontStyleTools from './FontStyleTools';
import ShapeTools from './ShapeTools';
import VideoTools from './VideoTools';

/**
 * This component renders the appropriate tools based on the active menu selection.
 * It dynamically displays either FontStyleTools, ShapeTools, or VideoTools depending on the active menu.
 *
 * @param {string} props.activeMenu - The currently active menu item name ('text', 'shape', or 'video').
 * @param {Object} props.fontStyle - The current font style settings.
 * @param {Function} props.handleAddText - Function to handle adding new text.
 * @param {Function} props.handleChangeText - Function to handle changes to the text style.
 * @param {Object} props.shapeStyle - The current shape style settings.
 * @param {Function} props.handleAddShape - Function to handle adding new shapes.
 * @param {Function} props.handleChangeShapeStyle - Function to handle changes to the shape style.
 * @param {string} props.aspectRatio - The current video aspect ratio.
 * @param {Function} props.handleAspectRatioChange - Function to handle changes to the video aspect ratio.
 *
 * @returns {JSX.Element} The rendered LeftToolbar component.
 */
const LeftToolbar = ({
  activeMenu,
  fontStyle,
  handleAddText,
  handleChangeText,
  shapeStyle,
  handleAddShape,
  handleChangeShapeStyle,
  aspectRatio,
  handleAspectRatioChange,
}) => {
  /**
   * Generates the appropriate submenu component based on the active menu item.
   *
   * @param {string} menu - The active menu item name.
   * @returns {JSX.Element} The corresponding submenu component.
   */
  const subMenuGenerator = (menu) => {
    const items = {
      text: (
        <FontStyleTools
          fontStyle={fontStyle}
          handleAddText={handleAddText}
          handleChangeText={handleChangeText}
        />
      ),
      shape: (
        <ShapeTools
          shapeStyle={shapeStyle}
          handleAddShape={handleAddShape}
          handleChangeShapeStyle={handleChangeShapeStyle}
        />
      ),
      video: (
        <VideoTools
          aspectRatio={aspectRatio}
          handleAspectRatioChange={handleAspectRatioChange}
        />
      ),
    };

    return items[menu];
  };

  return (
    <div className='left-toolbar-container'>{subMenuGenerator(activeMenu)}</div>
  );
};

export default LeftToolbar;
