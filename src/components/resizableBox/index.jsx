import React, { useRef, useState } from 'react';
import './resizableBox.css';

/**
 * ResizableBox component allows its children to be resized by the user.
 * The user can resize the box by dragging the resizer handle at the bottom-right corner.
 *
 * @param {React.ReactNode} props.children - The child elements to be rendered inside the resizable box
 * @param {React.ReactNode} props.initial - The initial states's values
 * @returns {JSX.Element} The rendered resizable box component
 */
const ResizableBox = ({ children, initial }) => {
  const [width, setWidth] = useState(initial.width);
  const [height, setHeight] = useState(initial.height);
  const rectRef = useRef(null);
  const isResizing = useRef(false);

  const startResizing = (e) => {
    e.preventDefault();
    e.stopPropagation();
    isResizing.current = true;
    document.addEventListener('mousemove', resize);
    document.addEventListener('mouseup', stopResizing);
  };

  const stopResizing = () => {
    isResizing.current = false;
    document.removeEventListener('mousemove', resize);
    document.removeEventListener('mouseup', stopResizing);
  };

  const resize = (e) => {
    if (isResizing.current) {
      const rect = rectRef.current;
      // Calculate new width and height based on mouse position
      const newWidth = e.clientX - rect.getBoundingClientRect().left;
      const newHeight = e.clientY - rect.getBoundingClientRect().top;

      // Ensure the new dimensions are above the minimum constraints
      if (newWidth > 50) {
        setWidth(newWidth);
      }
      if (newHeight > 50) {
        setHeight(newHeight);
      }
    }
  };

  // Clone and pass width and height props to children elements
  const childrenWithProps = React.Children.map(children, (child) => {
    // Checking isValidElement is the safe way and avoids a
    if (React.isValidElement(child)) {
      return React.cloneElement(child, {
        radius: width,
        width,
        height,
      });
    }
    return child;
  });

  return (
    <div
      className='resizable-container'
      ref={rectRef}
      style={{ width, height }}>
      <div className='resizer width' onMouseDown={startResizing} />
      <div className='resizer height' onMouseDown={startResizing} />
      {childrenWithProps}
    </div>
  );
};

export default ResizableBox;
