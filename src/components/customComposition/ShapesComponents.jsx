import { Rect, Circle } from '@remotion/shapes';

/**
 * shapeComponents object.
 *
 * This object maps shape names to their corresponding component implementations from the '@remotion/shapes' library.
 * It includes mappings for rectangles, squares, and circles.
 *
 * @property {React.Component} rect - The Rect component for rendering rectangles.
 * @property {React.Component} square - The Rect component used for rendering squares.
 * @property {React.Component} circle - The Circle component for rendering circles.
 *
 * @example
 * // Usage example:
 * import shapeComponents from './path-to-this-file';
 *
 * const MyComponent = () => {
 *   const Shape = shapeComponents['rect'];
 *   return <Shape {...props} />;
 * };
 */
const shapeComponents = {
  rect: Rect,
  square: Rect,
  circle: Circle,
};

export default shapeComponents;
