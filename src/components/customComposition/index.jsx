import { useState } from 'react';
import { AbsoluteFill, Video } from 'remotion';
import DraggableBox from '../draggableBox';
import TextMotion from '../editableText/TextMotion';
import ResizableBox from '../resizableBox';
import shapeComponents from './ShapesComponents';

/**
 * CustomComposition component combines a video and editable, draggable text.
 * @returns {JSX.Element} The CustomComposition component include a customized video.
 */
export const CustomComposition = ({
  videoUrl,
  texts,
  shapes,
  handleSelectShape,
  videoSize,
  handleSelectText,
  handleChangeText,
}) => {
  const [highestZIndex, setHighestZIndex] = useState(1);

  return (
    <>
      <AbsoluteFill style={{ alignItems: 'center' }}>
        <Video
          src={videoUrl}
          width={videoSize?.width}
          height={videoSize?.height}
        />
      </AbsoluteFill>

      {Object.entries(texts).map(([key, text]) => {
        return (
          <AbsoluteFill key={key}>
            <TextMotion
              fontStyle={texts[key]}
              value={text.value}
              handleChangeText={handleChangeText}
              onClick={() => handleSelectText(key)}
              highestZIndex={highestZIndex}
              setHighestZIndex={setHighestZIndex}
            />
          </AbsoluteFill>
        );
      })}

      {Object.entries(shapes).map(([key, shape]) => {
        const ShapeComponent = shapeComponents[shape.type];
        if (!ShapeComponent) {
          return null;
        }
        return (
          <DraggableBox
            key={key}
            highestZIndex={highestZIndex}
            setHighestZIndex={setHighestZIndex}>
            <AbsoluteFill>
              <ResizableBox initial={shape}>
                <ShapeComponent
                  id={key}
                  {...shape}
                  onClick={() => handleSelectShape(key)}
                />
              </ResizableBox>
            </AbsoluteFill>
          </DraggableBox>
        );
      })}
    </>
  );
};
