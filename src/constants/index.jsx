import { makeCircle, makeRect } from '@remotion/shapes';

export const CIRCLE = 'circle';
export const RECT = 'rect';
export const SQUARE = 'square';

// layer types:
export const TEXT = 'text';
export const SHAPE = 'shape';

export const LAYER_TYPE = { TEXT: 'text', SHAPE: 'shape' };

const { width: circleWidth, height: circleHeight } = makeCircle({ radius: 30 });
const { width: rectWidth, height: rectHeight } = makeRect({
  width: 200,
  height: 100,
});
const { width: squareWidth, height: squareHeight } = makeRect({
  width: 100,
  height: 100,
});

export const SHAPES_INITIAL_VALUE = {
  [CIRCLE]: {
    radius: 50,
    fill: '#000',
    width: circleWidth,
    height: circleHeight,
  },
  [RECT]: {
    width: rectWidth,
    height: rectHeight,
    fill: '#000',
  },
  [SQUARE]: { width: squareWidth, height: squareHeight, fill: '#000' },
};

export const STYLE_INITIAL_VALUE = {
  fontFamily: 'sans-serif',
  fontSize: '16',
  fontWeight: '400',
  color: '#000',
  gradient: '',
};

export const TEXT_INITIAL_VALUE = {
  value: 'Double click to edit!',
  ...STYLE_INITIAL_VALUE,
};

export const GRADIENT_DIRECTIONS = [
  { value: '90', label: 'To Right' },
  { value: '270', label: 'To Left' },
  { value: '0', label: 'To Top' },
  { value: '180', label: 'To Bottom' },
  { value: '45', label: 'To Top Right' },
  { value: '315', label: 'To Top Left' },
  { value: '135', label: 'To Bottom Right' },
  { value: '225', label: 'To Bottom Left' },
];

// Online video URL to test:'https://sample-videos.com/video321/mp4/720/big_buck_bunny_720p_10mb.mp4';
export const VIDEO_DEFAULT_VALUE = '../src/assets/videos/sample-video.webm';

export const ASPECT_RATIOS = ['16:9', '4:3', '1:1', '21:9', '9:16'];

// default fixed dimension (height) based on player height
export const FIXED_VIDEO_HEIGHT = 500;

export const FONT_WEIGHTS = ['100', '400', '600', '900'];

export const FONT_FAMILIES = ['sans-serif', 'cursive', 'arial', 'fantasy'];
