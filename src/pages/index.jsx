import './layout.css';
import Toolbar from '../components/toolbar';
import Container from '../components/container';
import { useState } from 'react';
import { calculateDimensions, getCurrentTimestamp } from '../utils';
import {
  FIXED_VIDEO_HEIGHT,
  LAYER_TYPE,
  SHAPES_INITIAL_VALUE,
  TEXT_INITIAL_VALUE,
  VIDEO_DEFAULT_VALUE,
} from '../constants';
import LeftToolbar from '../components/toolbar/LeftToolbar';
import Player from '../components/player';
import useDeleteKeyListener from '../hooks/useDeleteKeyListener';

export default function Home() {
  const [activeLayer, setActiveLayer] = useState({ id: '', type: '' });
  // text
  const [texts, setTexts] = useState({});
  // shape
  const [shapes, setShapes] = useState({});
  // video
  const [aspectRatio, setAspectRatio] = useState('4:3');
  // toolbar
  const [activeMenu, setActiveMenu] = useState('text');

  const handleDeleteKeyPress = () => {
    if (activeLayer.id === '') return;
    if (activeLayer?.type === LAYER_TYPE.TEXT) {
      const newTexts = { ...texts };
      delete newTexts[activeLayer.id];
      setTexts(newTexts);
      setActiveLayer({ id: '', type: '' });
    } else if (activeLayer?.type === LAYER_TYPE.SHAPE) {
      const newShapes = { ...shapes };
      delete newShapes[activeLayer.id];
      setShapes(newShapes);
      setActiveLayer({ id: '', type: '' });
    }
  };

  useDeleteKeyListener(handleDeleteKeyPress);

  /*********************** TOOLBAR ***********************/
  const handleChangeActiveMenu = (name) => setActiveMenu(name);

  /*********************** TEXT FUNCTIONS ***********************/
  const handleAddText = () => {
    const timeStamp = getCurrentTimestamp();
    setActiveLayer({ id: timeStamp, type: LAYER_TYPE.TEXT });
    setTexts((prev) => ({
      ...prev,
      [timeStamp]: TEXT_INITIAL_VALUE,
    }));
  };
  const handleSelectText = (id) =>
    setActiveLayer({ id, type: LAYER_TYPE.TEXT });

  const handleChangeText = (obj) => {
    const changed = { ...texts[activeLayer.id], ...obj };
    setTexts((prev) => ({
      ...prev,
      [activeLayer.id]: changed,
    }));
  };

  /*********************** SHAPE FUNCTIONS ***********************/
  const handleAddShape = (type) => {
    const timeStamp = getCurrentTimestamp();
    setActiveLayer({ id: timeStamp, type: LAYER_TYPE.SHAPE });

    setShapes((prevShapes) => ({
      ...prevShapes,
      [timeStamp]: { ...SHAPES_INITIAL_VALUE[type], type },
    }));
  };

  const handleSelectShape = (id) =>
    setActiveLayer({ id, type: LAYER_TYPE.SHAPE });

  const handleChangeShapeStyle = (obj) => {
    const changed = { ...shapes[activeLayer.id], ...obj };

    setShapes((prev) => ({
      ...prev,
      [activeLayer.id]: changed,
    }));
  };

  /*********************** VIDEO FUNCTIONS ***********************/
  const handleAspectRatioChange = (event) => {
    setAspectRatio(event.target.value);
  };

  const { width, height } = calculateDimensions(
    aspectRatio,
    FIXED_VIDEO_HEIGHT,
    false
  );

  /***************************************************************/
  return (
    <>
      <div className='toolbar'>
        <Toolbar
          activeMenu={activeMenu}
          handleChangeActiveMenu={handleChangeActiveMenu}
        />
      </div>

      <div className='main'>
        <div className='left-toolbar'>
          <LeftToolbar
            activeMenu={activeMenu}
            aspectRatio={aspectRatio}
            fontStyle={texts[activeLayer.id]}
            shapeStyle={shapes[activeLayer.id]}
            handleAddText={handleAddText}
            handleAddShape={handleAddShape}
            handleChangeShapeStyle={handleChangeShapeStyle}
            handleChangeText={handleChangeText}
            handleAspectRatioChange={handleAspectRatioChange}
          />
        </div>
        <div className='content'>
          <div className='layout'>
            <Container className='layout-container'>
              <Player
                inputProps={{
                  videoUrl: VIDEO_DEFAULT_VALUE,
                  texts,
                  shapes,
                  handleSelectShape,
                  handleSelectText,
                  handleChangeText,
                  videoSize: { width, height },
                }}
              />
            </Container>
          </div>
        </div>
      </div>
    </>
  );
}
