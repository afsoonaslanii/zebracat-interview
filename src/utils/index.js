/**
 * Function to calculate dimensions based on aspect ratio.
 *
 * @param {string} aspectRatio - The aspect ratio in the format "W:H".
 * @param {number} givenDimension - The given dimension (width or height).
 * @param {boolean} isWidthGiven - Flag indicating if the given dimension is width.
 *
 * @returns {Object} An object containing the calculated width and height.
 */
export function calculateDimensions(aspectRatio, givenDimension, isWidthGiven) {
  const [W, H] = aspectRatio.split(':').map(Number);

  if (isWidthGiven) {
    const width = givenDimension;
    const height = (width * H) / W;
    return { width, height };
  } else {
    const height = givenDimension;
    const width = (height * W) / H;
    return { width, height };
  }
}

/**
 * Function to get the current timestamp.
 *
 * @returns {number} The current timestamp in milliseconds since the Unix epoch.
 */
export function getCurrentTimestamp() {
  return Date.now();
}
