import { Composition } from 'remotion';
import { CustomComposition } from '../components/customComposition';

export const RemotionRoot = () => {
  const videoUrl =
    'https://sample-videos.com/video321/mp4/720/big_buck_bunny_720p_10mb.mp4';

  const fontStyle = {
    fontFamily: 'sans-serif',
    fontSize: '16',
    fontWeight: '400',
    color: 'black',
  };

  return (
    <>
      <Composition
        component={CustomComposition}
        durationInFrames={120}
        width={700}
        height={500}
        fps={30}
        id='composition'
        defaultProps={{ videoUrl, fontStyle }}
      />
    </>
  );
};
