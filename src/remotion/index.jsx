import { registerRoot } from 'remotion';
import { RemotionRoot } from './root';

/**
 * Entry point for registering the Remotion root component.
 *
 * This code registers the `RemotionRoot` component as the root component for Remotion,
 */
registerRoot(RemotionRoot);
