# Video Effects Application

## Description

The application enables users to enhance videos by adding customizable text and shapes (rectangles, square and circles). Users can modify the position, size, and style of these elements, as well as adjust the video's aspect ratio, creating a seamless and interactive video editing experience.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Example](#example)
- [Future Roadmap](#future-roadmap)
- [Contact Information](#contact-information)

## Installation

Before getting started, ensure you have the following prerequisites installed on your machine:

- Node.js (at least version 16)
- npm

Follow these steps to running the project:

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/afsoonaslanii/zebracat-interview.git
   ```
2. Navigate to the project directory:
   ```bash
   cd zebracat-interview
   ```
3. Install the dependencies:

   ```bash
   npm install
   ```

4. Run the application:

   ```bash
   npm run dev
   ```

## Usage

To use the application, navigate to the local server address provided in your terminal (usually `http://localhost:3000/`) in your web browser. The application provides the following functionality:

- **Add Text**: Insert text into the video and apply a variety of effects to enhance its appearance.
- **Change Text Position**: Easily adjust the position of the text on the video by dragging it with the mouse.
- **Add Shapes**: Add multiple shapes (rectangles, square and circles) to the video and change size of them.
- **Change Shapes Position**: Adjust the position of the shapes by dragging them with the mouse.
- **Manage Layers**: Allow users to manage layers of text and shapes by dragging elements to reorder them and bring them to the top as needed.
- **Remove Layers**: Easily remove layers by selecting them and pressing the Delete key on your keyboard.
- **Adjust Aspect Ratio**: Modify the aspect ratio of the video to fit different screen sizes and formats.

### Example

#### 1. **Working with Text**:

- **Adding Text**:
  - Select **Text** from the top sidebar and click **Add Text** in the left sidebar. You can add multiple text elements.
- **Customizing Text**:
  - Click on each text element to activate it, allowing customization of color, gradient, font weight, and more.
- **Editing Text**:
  - Double-click on a text element to edit its content.
- **Positioning Text**:

  - Drag and drop text elements to position them anywhere on the video.

_Note: Text elements are animated by default._

#### 2. **Working with Shapes**:

- **Adding Shapes**:
  - Select **Shape** from the top sidebar and choose a shape from the left sidebar (rectangles, squares, circles). Multiple shapes can be added.
- **Customizing Shapes**:
  - Click on each shape to activate it, enabling changes to its background color from the left sidebar.
- **Resizing Shapes**:
  - Resize shapes by using the green handle that appears when you hover over them. Handles are located differently based on the shape type (middle for circles, bottom-right for rectangles and squares).
- **Positioning Shapes**:
  - Drag and drop shapes to position them anywhere on the video.

#### 3. **Removing Layers**:

- **Deleting Layers**:
  - Click to select a layer.
  - Press the Delete key on your keyboard to remove it.

#### 4. **Adjusting Aspect Ratio**:

- **Changing Video Size**:
  - Select **Video** from the top sidebar to adjust aspect ratios available in the left sidebar dropdown menu. Choose a different aspect ratio to resize your video.

[Watch Demo 1](https://drive.google.com/file/d/1a9klLQBqNcwjy-k5-BG4jI-lhrWLj0mw/view)
[Watch Demo 2](https://drive.google.com/file/d/1Qg7cScQ9MbEKC1VO5Yul5g93AvCPIKdu/view)

## Future Roadmap

### Phase 1: Advanced Features and Enhancements

1. **Advanced Text and Shape Effects**: Implement more sophisticated text and shape effects (e.g., various animation types, transparency).
2. **Support for More Shape Types**: Add support for more shape types (e.g., polygons, lines).
3. **Layer Management**: Providing options to reorder, lock, and hide layers.
4. **Templates and Presets**: Create a library of templates and presets for users to apply quickly. Allow users to save and share their custom templates.
5. **Audio Integration**: Add functionality to include and edit audio tracks in the video

### Phase 2: Collaboration and Sharing

1. **Social Sharing**: Integrate with social media platforms for easy sharing of videos. Add options to export videos in formats optimized for various social media channels.

### Phase 3: Platform Expansion

1. **Mobile and Tablet Support**: Develop a responsive design for mobile and tablet devices. Create dedicated mobile apps for iOS and Android.

## Contact Information

For any questions or feedback, please contact me at:

- Email: afsoonaslanii@gmail.com
- LinkedIn: [afsoonaslanii](https://www.linkedin.com/in/afsoonaslanii/)
- GitLab: [afsoonaslanii](https://gitlab.com/afsoonaslanii)
